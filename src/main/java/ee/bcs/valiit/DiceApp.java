package ee.bcs.valiit;

import java.util.Random;

public class DiceApp {

    private static String first(int dice) {
        String first = new String();
        switch (dice) {
            case 1:
            case 2:
                first = "|       |";
                break;
            case 3:
                first = "| *     |";
                break;
            case 4:
            case 5:
            case 6:
                first = "| *   * |";
                break;
            default:
                break;
        }
        return first;
    }

    private static String second(int dice) {
        String second = new String();
        switch (dice) {
            case 4:
                second = "|       |";
                break;
            case 1:
            case 3:
            case 5:
                second = "|   *   |";
                break;
            case 2:
            case 6:
                second = "| *   * |";
                break;
            default:
                break;
        }
        return second;
    }

    private static String third(int dice) {
        String third = new String();
        switch (dice) {
            case 1:
            case 2:
                third = "|       |";
                break;
            case 3:
                third = "|     * |";
                break;
            case 4:
            case 5:
            case 6:
                third = "| *   * |";
                break;
            default:
                break;
        }
        return third;
    }


    public static void main(String[] args) {

        Random rand = new Random();
        int dice1 = rand.nextInt(6) + 1;
        int dice2 = rand.nextInt(6) + 1;
        int sum = dice1 + dice2;
        System.out.println("+-------+ +-------+");

        StringBuilder line1 = new StringBuilder();
        line1.append(first(dice1))
                .append(" ")
                .append(first(dice2));
        System.out.println(line1.toString());

        StringBuilder line2 = new StringBuilder();
        line2.append(second(dice1))
                .append(" ")
                .append(second(dice2))
                .append(" = " + sum);
        System.out.println(line2.toString());

        StringBuilder line3 = new StringBuilder();
        line3.append(third(dice1))
                .append(" ")
                .append(third(dice2));
        System.out.println(line3.toString());

        System.out.println("+-------+ +-------+");

    }


}
